module.exports = {
  collectCoverage: true,
  collectCoverageFrom: [
    'packages/**/src/**/*.{js,mjs}',
    '!**/node_modules/**',
  ],
  roots: [
    'packages/',
  ],
  moduleFileExtensions: ["js","mjs"],
  transform: {
    '^.+\\.(js|mjs)?$': require.resolve('babel-jest'),
  },
};
