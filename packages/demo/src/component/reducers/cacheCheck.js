const checkCache = (cache, ids) => {
  const hits = [];

  const keys = Object.keys(cache);
  if (keys.length === 0) {
    return { hits, misses: ids };
  }

  const misses = [];

  ids.map((id) => {
    if (keys.includes(id)) {
      hits.push(cache[id]);
    } else {
      misses.push(id);
    }
  });

  return { hits, misses };
};

export default checkCache;
