import fetch, { Headers } from "node-fetch";

const logout = (resp) => {
  if (
      resp.url.indexOf("/users/me") < 0 &&
      resp.url.indexOf("/auth") < 0
    ) {
    //console.log("Got a 401... logging you out");
    document.location.href = "/logout";
  }
};

class Client {

  constructor(props) {
    this.props = props;

    this.request = this.request.bind(this);
  }

  request(method, path, data, auth) {
    const {apiHost} = this.props;
    const cfg = {
      method,
      mode: "cors",
      cache: "no-cache",
      redirect: "follow"
    };
    if (data && method.toLowerCase() !== "get") {
      cfg.body = JSON.stringify(data);
    }
    const headers = {
      "Accept": "application/json",
      "Content-Type": "application/json"
    };
    if (auth) {
      headers.Authorization = `Basic ${btoa(`${auth.username}:${auth.password}`)}`;
    }
    cfg.headers = new Headers(headers);

    let uri = apiHost + path;
    if (method.toLowerCase() === "get" && data) {
      const queryString = Object.keys(data).map(key => {
        return `${encodeURIComponent(key)  }=${  encodeURIComponent(data[key])}`;
      }).join("&");
      uri += `?${  queryString}`;
    }

    return fetch(uri, cfg).then((resp) => {
      if (resp.status === 401) {
        return logout(resp);
      } else {
        return resp.json();
      }
    });
  }

}

export default Client;
