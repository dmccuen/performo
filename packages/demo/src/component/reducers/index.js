import {combineReducers} from "redux"
import cacheCheck from "./cacheCheck"
import demo from "./demo"
import { combinedInitialState } from './default/initialState'

// initialState
export const initialState = {
  demo: combinedInitialState,
  settings: {
    apiHost: "https://www.googleapis.com"
  }
};

const settings = (store) => {
  return store || initialState;
};

export default combineReducers({
  demo,
  settings
});

export { cacheCheck };

// Selectors
export const getDemoState = (state) => state.demo;
export const getDemoCache = (demoState) => venueState.byId;
export const getSelectedDemo = (demoState) => venueState.selected;
