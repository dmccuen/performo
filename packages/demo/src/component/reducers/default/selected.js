import { handleActions } from "redux-actions";
import getConstants from "./constants";
import {
  initialModelState
} from "./initialState";

const getSelected = (namespace) => {
  const {
    FETCHING, FETCHED, SAVE, SAVED,
    DELETE, DELETED, INVALIDATEMODEL
  } = getConstants(namespace);

  const selected = handleActions({
    [FETCHING]: (state) => {
      return Object.assign({}, state, {
        fetching: true,
        message: null,
        error: null
      });
    },
    [FETCHED]: (state, action) => {
      return Object.assign({}, state, {
        fetching: false,
        model: action.payload.response,
        message: action.payload.message,
        error: action.payload.error
      });
    },
    [SAVE]: (state) => {
      return Object.assign({}, state, {
        saving: true,
        message: null,
        error: null
      });
    },
    [SAVED]: (state, action) => {
      return Object.assign({}, state, {
        saving: false,
        model: action.payload.response,
        message: action.payload.message,
        error: action.payload.error
      });
    },
    [DELETE]: (state) => {
      return Object.assign({}, state, {
        deleting: true,
        message: null,
        error: null
      });
    },
    [DELETED]: (state, action) => {
      return Object.assign({}, state, {
        deleting: false,
        model: action.payload.response,
        message: action.payload.message,
        error: action.payload.error
      });
    },
    [INVALIDATEMODEL]: (state) => {
      return Object.assign({}, state, initialModelState);
    }
  },
  initialModelState
  );

  return selected;
};

export default getSelected;
