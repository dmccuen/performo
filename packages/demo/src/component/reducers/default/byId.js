import { handleActions } from "redux-actions";
import getConstants from "./constants";

const getById = (namespace) => {
  const { RECEIVE } = getConstants(namespace);

  const byId = handleActions({
    [RECEIVE]: (state, action) => {
      // create a map of ids to items
      return action.payload.hits.reduce((items, item) => {
        items[item.id] = item;
        return items;
      }, {});
    }
  },
  {}
  );

  return byId;
};

export default getById;
