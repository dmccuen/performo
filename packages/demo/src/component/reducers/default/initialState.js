export const initialState = {
  hits: [],
  total: 0,
  fetching: false,
  message: null,
  error: null
};

export const initialModelState = {
  model: null,
  fetching: false,
  saving: false,
  deleting: false,
  message: null,
  error: null
};

export const combinedInitialState = {
  query: initialState,
  selected: initialModelState,
  byId: {}
};
