import getActions from "./actions";

const getInvalidate = (namespace) => {
  const { invalidate } = getActions(namespace);

  const invalidateResults = () => {
    return (dispatch) => {
      return dispatch(invalidate());
    };
  };

  return invalidateResults;
};

export default getInvalidate;
