import getDeleteModel from "./delete";
import getFetchModel from "./fetch";
import getFetchIfNeeded from "./fetchIfNeeded";
import getInvalidate from "./invalidate";
import getInvalidateModel from "./invalidateModel";
import getSave from "./save";

const getActions = (namespace) => {
  const deleteModel = getDeleteModel(namespace);
  const fetchModel = getFetchModel(namespace);
  const queryIfNeeded = getFetchIfNeeded(namespace);
  const invalidateQuery = getInvalidate(namespace);
  const invalidateModel = getInvalidateModel(namespace);
  const saveModel = getSave(namespace);

  return {
    queryIfNeeded, invalidateQuery,
    fetchModel, saveModel, deleteModel, invalidateModel
  };
};

export default getActions;
