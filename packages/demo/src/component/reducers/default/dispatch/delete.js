import getActions from "./actions";
import Client from "../../client";
import { initialModelState } from "../initialState";

const getDelete = (namespace) => {
  const { deleting, deleted } = getActions(namespace);

  const deleteModel = (id) => {
    return (dispatch, getState) => {
      dispatch(deleting(id));

      const client = new Client(getState().settings);
      client.request("delete", `/${namespace}/${id}`)
        .then(() => {
          const state = Object.assign({},
            initialModelState,
            {message: "Deleted"});
          dispatch(deleted(state));
        })
        .catch(() => {
          dispatch(deleted({
            response: {},
            error: "Failed to delete."
          }));
        });
    };
  };

  return deleteModel;
};

export default getDelete;
