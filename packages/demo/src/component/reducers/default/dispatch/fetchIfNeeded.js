import Client from "../../client";
import getActions from "./actions";
import deepEqual from "deep-equal";
import { initialState } from "../initialState";

const getFetchIfNeeded = (namespace) => {
  const { request, receive } = getActions(namespace);

  const fetchQuery = (query, state) => {

    return (dispatch) => {

      dispatch(request(query));

      const client = new Client(state.settings);
      client.request(query.method, query.path, query.data)
        .then((json) => {
          const { items, searchInformation } = json
          dispatch(receive({
            hits: items,
            total: searchInformation.totalResults,
            query
          }));
        })
        .catch(() => {
          dispatch(receive(initialState));
        });

    };
  };

  const shouldFetch = (state, query) => {
    const {search} = state;
    if (!deepEqual(search, query)) {
      return true;
    }
    const results = state.themes.hits;
    return !results.length;
  };

  const fetchIfNeeded = (query) => {
    return (dispatch, getState) => {
      const state = getState();
      if (shouldFetch(state, query)) {
        return dispatch(fetchQuery(query, state));
      }
      return null;
    };
  };

  return fetchIfNeeded;
};

export default getFetchIfNeeded;
