import getActions from "./actions";
import Client from "../../client";

const getFetch = (namespace) => {
  const { fetching, fetched } = getActions(namespace);

  const fetchModel = (id) => {
    return (dispatch, getState) => {
      dispatch(fetching(id));

      const client = new Client(getState().settings);
      client.request("get", `/${namespace}/${id}`)
        .then((json) => {
          dispatch(fetched({
            response: json,
            message: null
          }));
        })
        .catch(() => {
          dispatch(fetched({
            response: null,
            error: "Failed to find model."
          }));
        });
    };
  };

  return fetchModel;
};

export default getFetch;
