import getActions from "./actions";
import Client from "../../client";

const getSave = (namespace) => {
  const { saving, saved } = getActions(namespace);

  const saveModel = (req) => {
    return (dispatch, getState) => {

      dispatch(saving(req));

      const client = new Client(getState().settings);
      client.request(req.method, req.path, req.data)
        .then((json) => {
          const mess = "Saved";
          let resp;
          if (json.message && json.message !== mess) {
            resp = {
              response: req.data,
              error: json.message
            };
          } else {
            resp = {
              response: json,
              message: "Saved"
            };
          }
          dispatch(saved(resp));
        })
        .catch(() => {
          dispatch(saved({
            response: req.data,
            error: "Save failed.  Please try again."
          }));
        });
    };
  };

  return saveModel;
};

export default getSave;
