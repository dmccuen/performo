import { createAction } from "redux-actions";
import getConstants from "../constants";

const getActions = (namespace) => {

  const {
    REQUEST, RECEIVE, INVALIDATE, SAVE, SAVED,
    DELETE, DELETED, FETCHING, FETCHED, INVALIDATEMODEL
  } = getConstants(namespace);

  // Actions
  const request = createAction(REQUEST);
  const receive = createAction(
    RECEIVE,
    payload => payload,
    () => ({receivedAt: Date.now()}));
  const invalidate = createAction(INVALIDATE);
  const saving = createAction(SAVE);
  const saved = createAction(SAVED);
  const deleting = createAction(DELETE);
  const deleted = createAction(DELETED);
  const fetching = createAction(FETCHING);
  const fetched = createAction(FETCHED);
  const invalidateModel = createAction(INVALIDATEMODEL);

  return {
    request, receive, invalidate, saving, saved, deleting,
    deleted, fetching, fetched, invalidateModel
  };

};

export default getActions;
