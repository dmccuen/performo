import getActions from "./actions";

const getInvalidate = (namespace) => {
  const { invalidateModel } = getActions(namespace);

  const invalidateSelectedModel = () => {
    return (dispatch) => {
      return dispatch(invalidateModel());
    };
  };

  return invalidateSelectedModel;
};

export default getInvalidate;
