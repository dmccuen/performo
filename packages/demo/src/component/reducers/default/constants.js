const get = (namespace) => {
  const REQUEST = `gwd/${namespace}/REQUEST`;
  const RECEIVE = `gwd/${namespace}/RECEIVE`;
  const INVALIDATE = `gwd/${namespace}/INVALIDATE`;
  const FETCHING = `gwd/${namespace}/FETCHING`;
  const FETCHED = `gwd/${namespace}/FETCHED`;
  const SAVE = `gwd/${namespace}/SAVE`;
  const DELETE = `gwd/${namespace}/DELETE`;
  const SAVED = `gwd/${namespace}/SAVED`;
  const DELETED = `gwd/${namespace}/DELETED`;
  const INVALIDATEMODEL = `gwd/${namespace}/INVALIDATEMODEL`;

  return {
    REQUEST, RECEIVE, INVALIDATE, FETCHING, FETCHED, SAVE,
    DELETE, SAVED, DELETED, INVALIDATEMODEL
  };
};

export default get;
