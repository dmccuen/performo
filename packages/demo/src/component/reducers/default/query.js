import { handleActions } from "redux-actions";
import getConstants from "./constants";
import {
  initialState
} from "./initialState";

const getQuery = (namespace) => {
  const {
    REQUEST, RECEIVE, INVALIDATE, SAVED, DELETED
  } = getConstants(namespace);

  const invalidate = (state) => {
    return Object.assign({}, state, initialState);
  };

  const query = handleActions({
    [REQUEST]: (state) => {
      return Object.assign({}, state, {
        fetching: true
      });
    },
    [RECEIVE]: (state, action) => {
      const val = Object.assign({}, state, {
        fetching: false,
        hits: action.payload.hits,
        total: action.payload.total
      });
      return Object.assign({}, state, val);
    },
    [INVALIDATE]: invalidate,
    [SAVED]: invalidate,
    [DELETED]: invalidate
  },
  initialState
  );

  return query;
};

export default getQuery;
