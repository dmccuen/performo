import { combineReducers } from "redux";
import getById from "./byId";
import getSelected from "./selected";
import getQuery from "./query";

const getReducer = (namespace) => {

  const query = getQuery(namespace);
  const selected = getSelected(namespace);
  const byId = getById(namespace);

  const reducer = combineReducers({ query, selected, byId });

  return reducer;
};

export default getReducer;
