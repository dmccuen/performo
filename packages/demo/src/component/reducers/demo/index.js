import getReducer from "../default";
import getActions from "../default/dispatch";

const namespace = "demo";

const reducer = getReducer(namespace);
export default reducer;

const {
  queryIfNeeded,
  fetchModel,
  saveModel,
  deleteModel,
  invalidateQuery,
  invalidateModel
} = getActions(namespace);

export {
  queryIfNeeded,
  invalidateQuery,
  fetchModel,
  saveModel,
  deleteModel,
  invalidateModel
};
