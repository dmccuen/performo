import { createStore, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import rootReducer, { initialState } from "./reducers";

const middleware = applyMiddleware(thunkMiddleware)
const store = createStore(rootReducer, initialState, middleware)

export default store
