import React from 'react'

const FunctionalHelloWorld = ({name}) => (
 <div>{`Hi ${name}`}</div>
)

export default FunctionalHelloWorld
