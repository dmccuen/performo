import React from 'react'
import _ from 'lodash'
import { getDemoState } from "./reducers";
import { queryIfNeeded, invalidateQuery } from "./reducers/demo";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

class ConnectedHelloWorld extends React.Component {

  componentDidMount() {
    this.search();
  }

  search() {
    const { getQuery } = this.props;
    getQuery({
      method: "get",
      path: "/customsearch/v1",
      data: {
        key: "AIzaSyD_L0lN9-E5BbnjiJou-wlPGCxiX8I8oTg",
        cx: "016289036601532464686:tq3ws25i_na",
        q: "beer"
      }
    });
  }

  render() {
    const { name, models } = this.props
    const list = _.get(models,"query.hits",[])
    return (
      <React.Fragment>
        <span>{ name }</span>
        <ul>
        {
          list.map((item,i) => <li key={i}>{i}</li>)
        }
        </ul>
      </React.Fragment>
    )
  }
}

ConnectedHelloWorld.defaultProps = {
  name: "foo"
}

const mapStateToProps = (state) => {
  return {
    models: getDemoState(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getQuery: queryIfNeeded,
    invalidate: invalidateQuery
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(ConnectedHelloWorld)
