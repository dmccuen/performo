import React from 'react'
import Hello from './hello'

class HelloWorld10 extends React.Component {
  render() {
    const { count, name } = this.props
    const hellos = []
    for (let i=0; i < count; i++) {
      hellos.push(<Hello key={i} name={name} />)
    }
    return <div>{ hellos }</div>
  }
}

HelloWorld10.defaultProps = {
  count: 10,
  name: "Doug"
}

export default HelloWorld10
