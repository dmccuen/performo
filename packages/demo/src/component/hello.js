import React from 'react'
import style from './hello.css'

class HelloWorld extends React.Component {
  constructor(props) {
    super(props);
    this.state = { color: "red" }
  }

  render() {
    const { name } = this.props
    return <span>{ name }</span>
  }
}

HelloWorld.defaultProps = {
  name: "foo",
  funky: ()=>{alert('funky')}
}

export default HelloWorld
