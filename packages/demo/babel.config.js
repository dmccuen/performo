module.exports = function (api) {
  api.cache(true)
  const presets = [
    "@babel/env",
    "@babel/react"
  ];
  const plugins = [
    "@babel/plugin-proposal-class-properties",
    '@babel/plugin-syntax-dynamic-import',
    "syntax-async-functions"
  ];

  return {
    presets,
    plugins
  };
}
