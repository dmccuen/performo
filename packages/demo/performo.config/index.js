const path = require('path')
const glob = require('glob')

const components = []

glob.sync('./performo.config/*.config.js').forEach( function(file) {
  const cfg = require( path.resolve(file) )
  components.push(cfg)
})

module.exports = {
  components,
  mongo: {
    uri: 'mongodb://localhost/performo',
    options: {
      db: {
        safe: true,
        debug: true
      }
    }
  },
  middleware: (data) => {
    if (!data) {
      console.error('no data for middleware')
      return
    }
    console.log(`middleware got the data for ${data.component.name}...`)

    const log = (mode, { name, phase, actualD, domNodeCount, dtd }) => {
      const data = { mode, name, phase, renderTime: actualD, domNodeCount }
      console.log(data)
      console.log(dtd)
    }

    log("render", data.renderRun.data[0])
    log("render", data.renderRun.data[1])
    log("hydration", data.hydrationRun.data[0])
    log("hydration", data.hydrationRun.data[1])

  },
  webpack: {
    config: {
      location: "webpack.config.js"
    }
  }
}
