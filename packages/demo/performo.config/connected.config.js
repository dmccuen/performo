module.exports = {
  name: "connected",
  budget: 3,
  specifier: "component/connected",
  useDefault: true,
  storeModule: "../performo.config/connectedStore",
  mountProps: {
    name: "foo"
  }
}
