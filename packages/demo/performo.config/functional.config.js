module.exports = {
  name: "functional",
  budget: 3,
  specifier: "component/functional",
  useDefault: true,
  mountProps: {
    name: "foo"
  },
  updateProps: {
    name: "bar"
  }
}
