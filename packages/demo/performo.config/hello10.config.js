module.exports = {
  name: "hello10",
  budget: 3,
  specifier: "component/10Hello",
  useDefault: true,
  mountProps: {
    name: "foo"
  },
  updateProps: {
    name: "bar"
  }
}
