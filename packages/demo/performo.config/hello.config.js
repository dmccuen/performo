module.exports = {
  name: "hello",
  budget: 3,
  specifier: "component/hello",
  useDefault: true,
  mountProps: {
    name: "foo"
  },
  updateProps: {
    name: "bar"
  }
}
