# Sample integration with Performo

Install the performo-harness
`npm install --save-dev @kemosabe7/performo-harness`

Install the babel dependencies if you don't already have them...

* babel-node
* babel.config.js

Create the `performo.config.js`

Add `performo` and `performo-debug` to your scripts in `package.json`

Run it with `npm run performo`
