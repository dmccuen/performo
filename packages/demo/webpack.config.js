const path = require('path');

module.exports = (env) => {

  const rootDir = process.cwd();
  const buildDir = "performo"

  return {
    entry: path.join(rootDir, buildDir, `__${env}.js`),
    output: {
      path: path.join(rootDir, buildDir),
      filename: `${env}.bundle.js`,
      libraryTarget: 'umd'
    },
    mode: 'development',
    optimization: {
      usedExports: true
    },
    resolve: {
      modules: [path.resolve(rootDir, 'src'), 'node_modules']
    },
    devServer: {
      contentBase: path.join(rootDir, 'src')
    },
    module: {
      rules: [
        {
          // this is so that we can compile any React,
          // ES6 and above into normal ES5 syntax
          test: /\.(js|jsx|mjs)$/,
          // we do not want anything from node_modules to be compiled
          exclude: /node_modules/,
          use: ['babel-loader']
        },
        {
          test: /\.(css|scss)$/,
          use: [
            "style-loader", // creates style nodes from JS strings
            "css-loader", // translates CSS into CommonJS
          ]
        },
        {
          test: /\.(jpg|jpeg|png|gif|mp3|svg|woff|woff2)$/,
          loaders: ['file-loader']
        }
      ]
    },
    plugins: []
  }
}
