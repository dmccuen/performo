#!/usr/bin/env node --experimental-modules --no-warnings

import cli from './cli'
const cmd = process.argv[2]
const componentName = process.argv[3]
cli({cmd, componentName})
