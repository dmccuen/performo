import { compile, run, clean, runSerial} from './harness'

const root = "node_modules/@kemosabe7/performo-harness"

const getDuration = (start) => () => {
  const end = Date.now()
  const dur = Math.floor((end - start)/1000);
  console.log(`Performo Duration: ${dur} seconds`)
}

const cli = ({cmd='harness', componentName, debug}) => {

  const startAt = Date.now()

  if (cmd === 'harness') {
    return compile(root, componentName)
      .then(() => run(componentName, debug))
      .then(clean)
      .then(getDuration(startAt))
      .catch((err)=>{
        console.error(`Performo Failed: ${err.message}`, err.stack)
      })
  }
  else if (cmd === 'clean') {
    return clean()
  }
  else if (cmd === 'compile') {
    return compile(root, componentName)
      .catch((err)=>{
        console.error(`Performo Failed to compile: ${err.message}`, err.stack)
      })
  }
  else if (cmd === 'run') {
    return run(componentName, debug)
  }
  else if (cmd === 'serial') {
    return runSerial(root, componentName, debug)
      .then(clean)
      .then(getDuration(startAt))
      .catch((err)=>{
        console.error(`Performo Failed: ${err.message}`, err.stack)
      })
  }
  else {
    console.log("Performo Error: unknown command")
  }
}

export default cli
