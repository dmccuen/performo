const getContent = async (page, options) => {
  const content = await page.evaluate((opts) => {
    const win = eval(opts.window)
    return win.document.querySelector('#app').innerHTML;
  }, options)
  .catch((err)=>{
    console.error(err.message,err.stack)
  })

  return content
}

export default getContent
