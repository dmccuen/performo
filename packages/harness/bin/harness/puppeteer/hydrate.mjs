const hydrateComponent = async (page, component, content, options) => {
  const result = await page.evaluate((comp, bMount, pageContent, opts) => {
    const win = eval(opts.window)
    const app = win.document.querySelector('#app')
    app.innerHTML = pageContent
    return win.renderComponent(comp, app, bMount, true);
  }, component, true, content, options)
  .catch((err)=>{
    console.error(err.message,err.stack)
    return false
  });

  return result
}

export default hydrateComponent
