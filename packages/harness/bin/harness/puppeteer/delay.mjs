const delay = async (ms) => {
  let delay = new Promise((resolve, reject) => {
    setTimeout(() => resolve("done!"), ms)
  });
  await delay;
}

export default delay
