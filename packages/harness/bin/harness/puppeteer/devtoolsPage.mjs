import puppeteer from 'puppeteer'
import path from 'path'
import init from './init'
import screenshot from './screenshot'
import { buildDir } from '../config'
import delay from './delay'

const getPage = async (browser, component, options) => {
  const page = await browser.newPage();
  const { name } = component
  await page.goto(`file:${path.resolve(`./${buildDir}/__devtools_${name}.html`)}`)

  page.on('console', msg => {
    if (msg.type === 'error') {
      console.error('PAGE LOG:', msg)
    }
  })

  await page.evaluate((opts) => {
    const win = eval(opts.window)
    const div = win.document.createElement("div")
    div.id = "app"
    win.document.getElementsByTagName("body")[0].append(div)
  }, options)

  await delay(500);

  await screenshot(page, component)
  await init(page, component, options)

  return page
}

export default getPage
