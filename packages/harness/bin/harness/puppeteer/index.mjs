import ReactDOMServer from 'react-dom/server';
import puppeteer from 'puppeteer'
import getPerformoPage from './page'
import getDevtoolsPage from './devtoolsPage'
import hydatePage from './hydrate'
import mountPage from './mount'
import updatePage from './update'
import reportData from './report'
import getContent from './content'

// NOTE this is the master switch
const useDevTools = true

const testFlow = async (component, debug) => {

  let page, data, content

  const browser = await puppeteer.launch({ headless: !debug, devtools: (debug) });

  if (debug) debugger

  const options = useDevTools ?
    { window: "window.frames[0]" }
    :
    { window: "window" }

  const getPage = useDevTools ? getDevtoolsPage : getPerformoPage

  // Render and Update
  page = await getPage(browser, component, options)
  await mountPage(page, component, options)
  content = await getContent(page, options)
  await updatePage(page, component, options)
  data = await reportData(page, component, options)
  const renderRun = { data }

  // Hydration
  page = await getPage(browser, component, options)
  await hydatePage(page, component, content, options)
  await updatePage(page, component, options)
  data = await reportData(page, component, options)
  const hydrationRun = { data }

  await browser.close();

  return { hydrationRun, renderRun, component }
}

export default testFlow
