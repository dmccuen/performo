import delay from './delay'

const updateComponent = async (page, component, options) => {
  const { storeModule } = component
  if (storeModule) {
    // connected components wait here
    // there could be many renders here
    await delay(2000);
  } else {
    // update phase
    await page.evaluate((comp, bMount, opts) => {
      const win = eval(opts.window)
      return win.renderComponent(comp, win.document.querySelector('#app'), bMount);
    }, component, false, options)
    .catch((err)=>{
      console.error(err.message,err.stack)
    });
  }
}

export default updateComponent
