const mountComponent = async (page, component, options) => {
  const result = await page.evaluate((comp, bMount, opts) => {
    const win = eval(opts.window)
    return win.renderComponent(comp, win.document.querySelector('#app'), bMount);
  }, component, true, options)
  .catch((err)=>{
    console.error(err.message,err.stack)
    return false
  });

  return result
}

export default mountComponent
