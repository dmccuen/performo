const initReportData = async (page, component, options) => {
  const data = await page.evaluate((comp, opts) => {
    const win = eval(opts.window)
    win.initData(comp);
  }, component, options)
  .catch((err)=>{
    console.error(err.message,err.stack)
  })

  return data
}

export default initReportData
