const reportData = async (page, component, options) => {
  const data = await page.evaluate((comp, opts) => {
    const win = eval(opts.window)
    return win.getData(win.document.querySelector('#app'));
  }, component, options)
  .catch((err)=>{
    console.error(err.message,err.stack)
  })

  return data
}

export default reportData
