import puppeteer from 'puppeteer'
import injectFile from '@entrptaher/puppeteer-inject-file'
import path from 'path'
import init from './init'
import screenshot from './screenshot'
import { buildDir } from '../config'

const getPage = async (browser, component, options) => {
  const page = await browser.newPage();

  await page.goto(`file:${path.resolve(`./${buildDir}/index.html`)}`)

  page.on('console', msg => {
    if (msg.type === 'error') {
      console.error('PAGE LOG:', msg)
    }
  })

  const { name } = component
  await injectFile(page, path.resolve(`./${buildDir}/${name}.bundle.js`));
  await screenshot(page, component)
  await init(page, component, options)

  return page
}

export default getPage
