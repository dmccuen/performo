import { buildDir } from '../config'
import delay from './delay'

let count = 0

const callback = async (page, component) => {
  await page.exposeFunction('takeScreenshot', async () => {
    const { name } = component
    const path = `./${buildDir}/__${name}_screenshot_${count++}.png`
    await delay(50);
    return page.screenshot({path})
  });
}

export default callback
