import config from '../../harness.config'

const getConfig = (componentName) => {
  return import(process.cwd() + "/performo.config")
    .then((config) => {
      const cfg = config.default
      if (componentName) {
        // reduce the components to just the one being requested
        cfg.components = cfg.components.filter((c) => c.name===componentName)
      }
      if (!cfg.webpack) cfg.webpack = {}
      return cfg
    })
    .catch((err) => {
      return new Error("Failed to load the performo config.",err.stack)
    })
}

const { buildDir } = config

export { buildDir }
export default getConfig
