// This was part of a PoC to see it you could run through the config faster by
// doing compile/run per component, instead of doing all the compiling then all
// the running like is coded in cli.mjs.  There doesn't seem be any difference
// at the moment...

import bundle from './compile/bundle'
import harness from '.'
import getConfig from './config'
import { startup, shutdown } from '@kemosabe7/performo-service'

const letsDoThis = async (rootDir, componentName, debug) => {

  await getConfig(componentName)
    .then(async ({components, mongo}) => {

      await startup(mongo)

      if (components && components.length) {
        for (var i=0; i<components.length; i++) {
          const component = components[i]
          await bundle(component, rootDir)
            .then((c) => harness(c, debug))
            .catch(e => {
              const { name } = component
              console.error(`Performo failed to test component ${name}`,e.stack)
            })
        }
      } else {
        console.log("Performo: Please define some components in performo.config.js")
      }

      await shutdown()

    })
}

export default letsDoThis
