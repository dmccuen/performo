import { exec } from "child_process"
import config from '../../harness.config'

const clean = () => {
  return new Promise((resolve, reject) => {
    const { buildDir } = config
    exec(`rm -rf ${buildDir}/*`, (err) => {
      if (err) reject(err)
      else resolve()
    })
  })
}

export default clean
