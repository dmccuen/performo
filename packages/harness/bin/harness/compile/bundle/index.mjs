import bundleConnectedComponent from './connected'
import bundleComponent from './default'

const bundle = async (component, rootDir, webpack) => {
  const { storeModule } = component
  const handleComponent = (storeModule) ? bundleConnectedComponent : bundleComponent
  const result = handleComponent(component, rootDir, webpack)
  await result
  return component
}

export default bundle
