import writeEntryPointFile from './write'
import createBundleForComponent from './webpack'
import devtools from './devtools'
import fs from 'fs'
import path from 'path'

const bundleConnectedComponent = (component, rootDir, webpack) => {
  return new Promise((resolve, reject) => {
    const template = path.join(rootDir, 'bin','harness','compile','template','app.jsx')
    const promise = fs.promises.open(template, 'r')
    promise.then(async (file) => {
        const buf = await file.readFile()
        const str = buf.toString()
        await file.close()
        return str
      })
      .then((str) => {
        const { useDefault, namedModule, specifier, storeModule } = component
        let content = str
        let importStr = (useDefault) ?
          `import Component from '${specifier}'`
          :
          `import { ${namedModule} as Component } from '${specifier}'`
        content = content.replace("%COMPONENT_SPECIFIER%", importStr)
        importStr = `import store from '${storeModule}'`
        content= content.replace("%STORE_SPECIFIER%", importStr)

        return content
      })
      .then(writeEntryPointFile(component, rootDir))
      .then(createBundleForComponent(component, rootDir, webpack))
      .then(devtools(component, rootDir))
      .catch(reject)
      .then(resolve)

    return promise
  })
}

export default bundleConnectedComponent
