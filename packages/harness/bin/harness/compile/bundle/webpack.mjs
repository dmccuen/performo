import { spawn } from 'child_process'
import _ from 'lodash'

const createBundleForComponent = (component, rootDir, cfg) => () => {
  
  const promise = new Promise((resolve, reject) => {

    const { name } = component

    // NOTE: May want to review the location of the Babel dependencies...
    // I'm using the babel config and dependencies in the demo package.
    // The harness doesn't have any opinions about Babel, but it does need
    // babel-node in the Path to in order to spawn the child process.

    // allow an override for the webpack.config
    const webpackConfig = _.get(
      cfg,
      "config.location",
      `${rootDir}/webpack.config.js`
    )

    const webpack = spawn(
      'babel-node',
      [
        `./node_modules/webpack/bin/webpack`,
        `--env=${name}`,
        '--config',
        webpackConfig
      ]
    )

    webpack.stdout.on('data', (data) => {
      console.log(`webpack: ${data}`);
    });

    //webpack.stderr.on('data', (data) => {
      //console.log(`webpack message: ${data}`);
      //reject(new Error(`webpack error in component ${name}`))
    //});

    webpack.on('close', (code) => {
      if (code!==0) {
        console.log(`webpack process for ${name} exited with code ${code}`)
      }
      resolve(component)
    });

    webpack.on('error', ( err ) => {
      console.log(`webpack err: ${err}`);
      reject(new Error(`webpack error in component ${name}`))
    })

  })

  return promise
}

export default createBundleForComponent
