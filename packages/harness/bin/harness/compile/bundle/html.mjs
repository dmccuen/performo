import fs from 'fs'
import path from 'path'
import { buildDir } from '../../config'

const writeHTMLFile = (rootDir) => async () => {
  // create the build dir if it doesn't exist
  if (!fs.existsSync(buildDir)) {
    await fs.promises.mkdir(buildDir)
  }

  const shellDir = path.join(buildDir, "shell")
  if (!fs.existsSync(shellDir)) {
    await fs.promises.mkdir(shellDir)
  }

  // copy the file...
  const promises = []
  promises.push(copyFile(rootDir,'index.html'))
  promises.push(copyFile(rootDir,'shell/backend.js'))
  promises.push(copyFile(rootDir,'shell/container.js'))

  return Promise.all(promises)

}

const copyFile = (rootDir, filePath) => {
  const input = path.join(rootDir, 'bin','harness','compile','template',filePath)
  const output = path.join(buildDir, filePath)
  const file = fs.promises.copyFile(input, output).catch(e => {
    console.log(`Performo failed to write ${filePath}`, e.stack)
  })
  return file
}

export default writeHTMLFile
