import fs from 'fs'
import path from 'path'
import { buildDir } from '../../config'

const writeDevToolsEntryPointFile = (component, rootDir) => async (content) => {
  // create the build dir if it doesn't exist
  if (!fs.existsSync(buildDir)) {
    await fs.promises.mkdir(buildDir)
  }

  // write the entry point file for webpack
  const { name } = component
  const filePath = path.join(buildDir, `__devtools_${name}.html`)
  const file = fs.promises.open(filePath, 'w')
  file.then(async (epf) => {
    await epf.writeFile(content)
    await epf.close()
    return component
  })
  .catch(e => {
    console.log(`Performo failed to write devtools entry point for ${name}`, e.stack)
  })

  return file
}

export default writeDevToolsEntryPointFile
