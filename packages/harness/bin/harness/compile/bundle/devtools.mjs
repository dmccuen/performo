import writeEntryPointFile from './writeDevtools'
import fs from 'fs'
import path from 'path'

const buildDevtoolsContainer = (component, rootDir) => () => {

  return new Promise((resolve, reject) => {
    const template = path.join(rootDir, 'bin','harness','compile','template','devtools.html')
    const promise = fs.promises.open(template, 'r')
    promise.then(async (file) => {
        const buf = await file.readFile()
        const str = buf.toString()
        await file.close()
        return str
      })
      .then((str) => {
        const { name } = component
        return str.replace("%NAME%", name)
      })
      .then(writeEntryPointFile(component, rootDir))
      .catch(reject)
      .then(resolve)

    return promise
  })

}

export default buildDevtoolsContainer
