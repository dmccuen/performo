import writeEntryPointFile from './write'
import createBundleForComponent from './webpack'
import devtools from './devtools'
import fs from 'fs'
import path from 'path'

const bundleComponent = (component, rootDir, webpack) => {

  return new Promise((resolve, reject) => {
    const template = path.join(rootDir, 'bin','harness','compile','template','component.jsx')
    const promise = fs.promises.open(template, 'r')
    promise.then(async (file) => {
        const buf = await file.readFile()
        const str = buf.toString()
        await file.close()
        return str
      })
      .then((str) => {
        const { useDefault, namedModule, specifier } = component
        const importStr = (useDefault) ?
          `import Component from '${specifier}'`
          :
          `import { ${namedModule} as Component } from '${specifier}'`
        return str.replace("%COMPONENT_SPECIFIER%", importStr)
      })
      .then(writeEntryPointFile(component, rootDir))
      .then(createBundleForComponent(component, rootDir, webpack))
      .then(devtools(component, rootDir))
      .catch(reject)
      .then(resolve)

    return promise
  })

}

export default bundleComponent
