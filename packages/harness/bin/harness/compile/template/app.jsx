import React, { unstable_Profiler as Profiler } from 'react'
import { render, hydrate } from 'react-dom'
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "react-redux";
import log, { initData, getData, agent } from '@kemosabe7/performo-harness'
%COMPONENT_SPECIFIER%
%STORE_SPECIFIER%

class EntryPoint extends React.Component {

  constructor(props) {
    super(props);
    this.handleLog = this.handleLog.bind(this)
    this.component = React.createRef()

    // this.perf.ready will contain the resolve method for a promise after
    // the renderComponent method is called.
    this.perf = {
      ready: () => {
        console.log('this is not the data you were looking for...')
      }
    }
  }

  // this method logs the data, but also resolves the renderComponent promise
  handleLog(id, phase, actualD, baseD, startT, commitT) {
    let props = this.component.current.props
    let componentName = Component.displayName || Component.name || 'Component';
    try {
      const c = this.component.current.getWrappedInstance()
      props = c.props
      componentName = c.constructor.name
    } catch (e) {
      console.log(`connected component, ${id}, needs to add { withRef: true }`)
    }
    const defaults = { actualD, baseD, startT, commitT, props }
    const { perf } = this
    window.takeScreenshot().then(screenshot => {
      const data = { id, phase, screenshot }
      log(window)(data, componentName, defaults)
      perf.ready(data)
    }).catch(console.error)
  }

  render() {
    const { id, props } = this.props
    return (
      <Profiler id={id} onRender={this.handleLog}>
        <Component ref={this.component} { ...props }/>
      </Profiler>
    )
  }
}

EntryPoint.defaultProps = {
  id: "foo",
  props: {}
}

export default EntryPoint

const appStart = (node, isHydrate, appProps) => {

  const reactStart = (isHydrate) ? hydrate : render

  return new Promise((resolve, reject) => {
    let ref;
    try {
      reactStart(
        <Provider store={store}>
          <Router>
            <EntryPoint { ...appProps } ref={ (e) => ref = e } />
          </Router>
        </Provider>,
        node,
        () => {
          // the render callback gives me a chance to set the resolve
          // method on the component, so this promise can be resolved after
          // the data is logged.
          const { perf } = ref
          perf.ready = resolve
        }
      );

    } catch (e) {
      reject(e);
    }
  });
}

// this returns a promise that is resolved after the logging is done.
window.renderComponent = (comp, appNode, bMount, isHydrate) => {

  const { name, mountProps } = comp
  const appProps = {
    id: name,
    props: mountProps
  }

  return appStart(appNode, isHydrate, appProps)
}

window.getData = getData(window)
window.initData = initData(window)
window.getDevToolsData = agent(window.__REACT_DEVTOOLS_GLOBAL_HOOK__)
