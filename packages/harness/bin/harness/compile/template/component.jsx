import React, { unstable_Profiler as Profiler } from 'react'
import { render, hydrate } from 'react-dom'
import { BrowserRouter as Router } from "react-router-dom";
import log, { getData, initData, agent } from '@kemosabe7/performo-harness'
%COMPONENT_SPECIFIER%

class EntryPoint extends React.Component {

  constructor(props) {
    super(props)
    this.handleLog = this.handleLog.bind(this)
    this.component = React.createRef()

    // this.perf.ready will contain the resolve method for a promise after
    // the renderComponent method is called.
    this.perf = {
      ready: () => {
        console.log('this is not the data you were looking for...')
      }
    }
  }

  // this method logs the data, but also resolves the renderComponent promise
  handleLog(id, phase, actualD, baseD, startT, commitT) {
    const props = (this.isStateless()) ?
      this.props.props
      :
      this.component.current.props
    const defaults = { actualD, baseD, startT, commitT, props }
    const { perf } = this
    window.takeScreenshot().then(screenshot => {
      const data = {id, phase, screenshot}
      const componentName = Component.displayName || Component.name || 'Component';
      log(window)(data, componentName, defaults)
      perf.ready(data)
    }).catch(console.error)
  }

  isStateless() {
    return !Component.prototype || !Component.prototype.render
  }

  render() {
    const { id, props } = this.props
    const comp = (this.isStateless()) ?
      <Component { ...props }/>
      :
      <Component ref={this.component} { ...props }/>

    return (
      <Router>
        <Profiler id={id} onRender={this.handleLog}>
          { comp }
        </Profiler>
      </Router>
    )
  }
}

EntryPoint.defaultProps = {
  id: "foo",
  props: {}
}

export default EntryPoint

// this returns a promise that is resolved after the logging is done.
window.renderComponent = (comp, node, bMount, bHydrate) => {

  const { name, mountProps, updateProps } = comp
  const props = (bMount) ? mountProps : updateProps
  const reactStart = (bHydrate) ? hydrate : render

  return new Promise((resolve, reject) => {
    let ref;
    try {
      reactStart(
        <EntryPoint id={name} props={props} ref={ (e) => ref = e } />,
        node,
        () => {
          // the render callback gives me a chance to set the resolve
          // method on the component, so this promise can be resolved after
          // the data is logged.
          const { perf } = ref
          perf.ready = resolve
        }
      );
    } catch (e) {
      reject(e);
    }
  });
}

window.getData = getData(window)
window.initData = initData(window)
window.getDevToolsData = agent(window.__REACT_DEVTOOLS_GLOBAL_HOOK__)
