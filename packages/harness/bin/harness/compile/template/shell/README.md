# Forking the Plain Shell example

This [Plain Shell example](https://github.com/facebook/react-devtools/tree/master/shells/plain) has a way to capture the devtools data set, but should be customized for Performo.  The `backend` and `container` files here have been built from the source at the link above.  Ideally, that would be custom fork for Kroger that would be hosted in their NPM repository, so it could be imported as a dependency in the Performo template.

This needs to be loaded into a different html template...
