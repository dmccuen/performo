import bundle from './bundle'
import getConfig from '../config'
import writeHTMLFile from './bundle/html'

const bundleComponents = (rootDir, componentName) => {
  return new Promise((resolve,reject) => {
    getConfig(componentName)
      .then(async ({ components, webpack }) => {
        for (var i=0; i<components.length; i++) {
          await bundle(components[i], rootDir, webpack)
        }
      })
      .then(writeHTMLFile(rootDir))
      .then(resolve)
      .catch(reject)
  })
}

export default bundleComponents
