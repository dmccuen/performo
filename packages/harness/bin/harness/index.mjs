import persist from '@kemosabe7/performo-service'
import compile from './compile'
import run from './run'
import runSerial from './serial'
import puppeteer from './puppeteer'
import clean from './clean'
import middleware from './middleware'

const testHarness = async (component, debug) => {

  console.log(`Performo starting run for ${component.name}`)

  const promise = puppeteer(component, debug)
    .then(data => (!debug) ? persist(data) : null)
    .then(middleware)
    .catch(e => {
      console.error(`Performo failed run for ${component.name}`, e.stack)
    })

  return promise
}

export default testHarness

export { compile, run, clean, runSerial }
