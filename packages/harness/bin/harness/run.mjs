import harness from '.'
import getConfig from './config'
import { startup, shutdown } from '@kemosabe7/performo-service'

const letsDoThis = async (componentName, debug) => {

  await getConfig(componentName)
    .then(async ({components, mongo}) => {

      await startup(mongo)

      if (components && components.length) {
        for (var i=0; i<components.length; i++) {
          await harness(components[i], debug);
        }
      } else {
        console.log("Performo: Please define some components in performo.config.js")
      }

      await shutdown()

    })

}

export default letsDoThis
