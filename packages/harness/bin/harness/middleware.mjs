import getConfig from './config'

const middleware = (data) => {

  return getConfig()
    .then(config => {
      const { middleware } = config
      if (middleware) middleware(data)
      return data
    })

}

export default middleware
