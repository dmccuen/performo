#!/usr/bin/env ndb node --experimental-modules --no-warnings

import cli from './cli'
const cmd = process.argv[2]
const componentName = process.argv[3]
if (!cmd || !componentName) {
  console.log("Performo Error: Debugging requires a command and component name.")
} else {
  cli({cmd, componentName, debug: true})
}
