import log from '.'

const key = 'profileData'
const window = {
  getDevToolsData: (names) => {
    return [{name: "foo", nodeType: "foo"}]
  }
}

let data, id, name, defaults, nodeType, phase, actualDuration, treeBaseDuration, actualStartTime, props

beforeEach(() => {
  window[key] = []
  id = "foo"
  phase = "bar"
  actualDuration = 1
  treeBaseDuration = 2
  actualStartTime = 3
  props = {}
  name = 'foo'
  nodeType = 'foo'
  defaults = {}
  data = {id, phase, actualDuration, treeBaseDuration, actualStartTime, props}
})

describe('log', () => {

  test('data is logged', () => {
    log(window)(data, name, defaults)
    const run = window[key][0]
    expect(run.name).toEqual(id)
  })

})
