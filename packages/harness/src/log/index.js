import _ from 'lodash'
const key = 'profileData'

const log = (window) => (run, name, defaults) => {

  const { id, phase, screenshot } = run
  const { actualD, baseD, startT, props } = defaults
  const node = document.querySelector("#app")
  const dtd = window.getDevToolsData()
  let componentTiming = _.find(dtd, {name})

  window[key].push({
    configId: id,
    name,
    nodeType: (componentTiming)?componentTiming.nodeType:null,
    phase,
    actualDuration: (componentTiming)?componentTiming.actualDuration:actualD,
    treeBaseDuration: (componentTiming)?componentTiming.treeBaseDuration:baseD,
    actualStartTime: (componentTiming)?componentTiming.actualStartTime:startT,
    domNodeCount: node ? node.getElementsByTagName("*").length : 0,
    props: (componentTiming)?componentTiming.props:props,
    state: (componentTiming)?componentTiming.state:{},
    screenshot,
    dtd
  })
}

const initData = (window) => () => {
  window[key] = []
}

const getData = (window) => () => {
  return window[key]
}

export { initData, getData }
export default log
