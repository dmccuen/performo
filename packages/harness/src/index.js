import Log, { getData, initData } from './log'
import agent from './devtools/agent'

export default Log
export { getData, initData, agent }
