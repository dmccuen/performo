# Performo - Test Harness

The Test Harness compiles a configured react component with WebPack and injects the bundle into a Chromium page with Puppeteer.  It then uses Puppeteer to render and update the component with and without SSR.  The performance profile for the component is captured with the React 16.5 Profiler, and persisted to MongoDB.


## Challenges

* The test harness can only pass args that are serializable to `renderComponent`.  That means the component to be rendered can't be passed in.
* Given that the component can't be passed to `renderComponent`, the component must be part of the bundle injected in the Chromium page.


## Additional Details

* The data from the profiler is stored on the window object, and passed back to the test harness.  That data must be serializable.
