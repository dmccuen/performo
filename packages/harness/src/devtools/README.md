# Embedding React DevTools into Performo...

The React Profiler dev tool is able to pull timings for all components.  How do they instrument all the components?  Is that part of the dev build?  Is it possible to tap into that data when profiling the component in the harness?

* [DevTools](https://github.com/facebook/react-devtools)
* [The Agent](https://github.com/facebook/react-devtools/blob/master/agent/Agent.js)
* [The Backend](https://github.com/facebook/react-devtools/blob/master/shells/plain/backend.js)
* [The Shell Example](https://github.com/facebook/react-devtools/tree/master/shells/plain)

That last one could be awesome...  It could give me the way to profile all the components...

* testing large composed components for a team, rather than their small components...
* the data from this thing is way more robust.  No screenshot or "domNodeCount", but the react data is much better and deeper.


 ## What's going on here...

 I haven't been able to embed the standalone module, but it seems like that should work.

 The "plain" shell is easier to pull off (given the example they provide), but it makes Performo even more complex than it already is.
