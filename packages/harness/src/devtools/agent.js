import _ from 'lodash'

const copyObject = (data) => {
  const message = "cleaned by performo"
  const keys = _.without(_.keys(data), "children")
  const obj = {}
  keys.forEach((key) => {
    const d = data[key]
    // sniff out the problem values...
    if (!_.isObject(d)) {
      obj[key] = d
    } else if (Array.isArray(d)) {
      const cleanArray = []
      d.forEach((val) => {
        if (!_.isObject(val)) {
          cleanArray.push(val)
        } else {
          cleanArray.push(message)
        }
      })
      obj[key] = cleanArray
    } else {
      obj[key] = message
    }
  })
  return obj
}

const getDevToolsData = (hook) => () => {
  const { reactDevtoolsAgent } = hook
  if (reactDevtoolsAgent && reactDevtoolsAgent.elementData) {
    const { elementData } = reactDevtoolsAgent
    const keys = [...elementData.keys()]
    const results = []
    keys.forEach((key)=>{
      const value = elementData.get(key)
      const { name, nodeType, actualDuration, actualStartTime, treeBaseDuration, props, state } = value
      const d = { name, nodeType, actualDuration, actualStartTime, treeBaseDuration }
      // the props and state can be huge, so clean it up (shallow copy)
      if (props) d.props = copyObject(props)
      if (state) d.state = Object.assign({}, state)
      results.push(d)
    })
    return results
  }
  return false
}

export default getDevToolsData
