# Performo - Test Harness

This is a test harness and for automated monitoring of react components.  It is based on the Profiler work in React 16.5: https://github.com/reactjs/rfcs/pull/51


## Configuring

First, create `performo.config.js` to define the components to test.  This file should be located where you are running performo.  That is generally the root of the package that contains the components to be tested.  See the demo package for an example.

Second, add the `performo` and `performo-debug` scripts to `package.json`.  Again, see the demo package for an example.


## Running

`npm run performo`

That will run 3 tasks:

* **compile**: This loops over the components in the config, and generates a test file and webpack bundle for each of them.  See `src/harness/compile/index.mjs`.
* **run**: This loops over the components in the config, and sends each into the test harness.  See `src/harness/index.mjs`.
* **clean**: This deletes all the temp files from `compile` and `run`.

You can also run the tasks separately as follows:

`npm run performo compile`

`npm run performo run`

`npm run performo clean`


## Debugging

Similar to above, but more specific.  Options include:

`npm run performo-debug harness <component-name>`

`npm run performo-debug compile <component-name>`

`npm run performo-debug run <component-name>`

`npm run performo-debug clean <component-name>`

This runs node with `ndb`, so set your debugger breakpoints as needed.  It will also run Chromium in debug mode during the `run` task.
