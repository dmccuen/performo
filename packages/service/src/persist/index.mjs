import Performance from './mongo/model'
import mongoose, { initMongoose } from './mongo/mongoose'

const shutdown = () => {
  // mongoose.disconnect() Runs .close() on all connections in parallel.
  return mongoose.disconnect().catch(console.error)
}
export { shutdown }

const getConnection = () => {
  return mongoose.connection
}
export { getConnection }

const startup = ({uri, options}) => {
  initMongoose(options)
  const promise = new Promise((resolve, reject) => {
    const connectionOptions = {
      useNewUrlParser: true,
      autoIndex: true, // Don't build indexes
      reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
      reconnectInterval: 500, // Reconnect every 500ms
      poolSize: 10, // Maintain up to 10 socket connections
      // If not connected, return errors immediately rather than waiting for reconnect
      bufferMaxEntries: 0,
      bufferCommands: false,
      connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
      socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
      family: 4 // Use IPv4, skip trying IPv6
    }
    mongoose.connect(uri, connectionOptions).catch(console.warn).then(resolve)
  })

  return promise
}
export { startup }

const logger = async (data) => {
  const { hydrationRun, renderRun, component } = data
  const runId = mongoose.Types.ObjectId()
  const { budget } = component
  await persist(hydrationRun, budget, true, runId)
  await persist(renderRun, budget, false, runId)
  return data
}

const persist = async ({ data }, budget, bHydrate, runId) => {

  if (!data) {
    throw new Error(`No data to persist from runId ${runId}.`)
  }

  for (var i=0; i<data.length; i++) {
    const run = data[i]
    const { configId, name, nodeType, phase, actualDuration, treeBaseDuration, actualStartTime, domNodeCount, props, state, screenshot, dtd } = run
    let passed = null
    if (budget) {
      passed = (budget > actualDuration)
    }

    const defaults = {
      runId,
      configId,
      phase,
      mode: (bHydrate) ? "hydrate":"render"
    }

    const specifics = {
      name,
      nodeType,
      domNodeCount,
      actualDuration,
      treeBaseDuration,
      actualStartTime,
      passed,
      screenshot,
      props,
      state,
    }

    await Performance.create(Object.assign({}, defaults, specifics))

    for (var i=0; i<dtd.length; i++) {
      const component = dtd[i]
      await Performance.create(Object.assign({}, defaults, dtd[i]))
    }
  }

  return true
}

export default logger
