import mongoose from 'mongoose'
const Schema = mongoose.Schema
const MOUNT = 'mount'
const UPDATE = 'update'
export const phaseTypes = [MOUNT, UPDATE]
const RENDER = 'render'
const HYDRATE = 'hydrate'
export const modeTypes = [RENDER, HYDRATE]

const performanceSchema = new Schema({
  phase: {
    type: String,
    enum: phaseTypes,
    default: MOUNT
  },
  runId: {
    type: Schema.Types.ObjectId,
    required: true
  },
  actualDuration: Number,
  treeBaseDuration: Number,
  actualStartTime: Number,
  domNodeCount: Number,
  configId: String,
  name: String,
  nodeType: String,
  screenshot: Buffer,
  passed: Boolean,
  props: Schema.Types.Mixed,
  state: Schema.Types.Mixed,
  mode: {
    type: String,
    enum: modeTypes,
    default: RENDER
  }
}, {
  timestamps: true
})

performanceSchema.methods = {
  view (full) {
    let view = {}
    let fields = ['id', 'runId', 'configId', 'phase', 'mode', 'name', 'passed', 'nodeType', 'actualDuration', 'treeBaseDuration', 'domNodeCount' ]

    if (full) {
      fields = [...fields, 'screenshot', 'props', 'state', 'actualStartTime', 'createdAt', 'updatedAt' ]
    }

    fields.forEach((field) => { view[field] = this[field] })

    return view
  }
}

const model = mongoose.model('Performance', performanceSchema)

export const schema = model.schema
export default model
