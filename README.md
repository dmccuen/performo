# Performo

React Component Performance Testing and Analysis

## Why

***Performance Budgets***
These are generally defined as budgets for an entire page.  When a page blows through its budget, alerts can fire, but who is responsible for finding the best way to address the problem?  Performance budgets by themselves do little to improve performance, or instill "culture of performance" is large development team.  Performo aims to help bridge this gap by providing metrics at the React Component level.  This is where development happens, and alerts at this level are more actionable.

Is performance additive?  Sometimes.  If you are talking about K-weight or Dom-node-count, then performance is additive.  If you are talking about rendering performance, then it is not.  If one button renders in 3ms, will 10 buttons render in 30ms?  No.  Not even close.  Predicting rendering performance of a component may be impossible.  You need to test in order to get the actual cost of a component.

If you can't predict rendering performance, why should you test a component in isolation?  Rendering performance is also about identifying wasted renders.  It is also about capturing the cost of mapStateToProps for a connected component.

https://gist.github.com/bvaughn/8de925562903afd2e7a12554adcdda16

It is best to monitor performance of production code, but the webpack process makes monitoring JS performance incredibly difficult with the existing tools.  You can see slowdowns, but obfuscation makes it difficult to see which component is responsible.

It would be better to have an automated tool to monitor components to determine which are heavy or slow.  Different components will have different expectations, but the important thing is to be able to see the trends for change over time.  Has this component been getting slower?  Has that component been getting heavier?

Why does render performance change so much for a given component?  Is there a "warm up" phase that makes later renders faster, even on a new page?


## Installation

```bash
npm install
npm run bootstrap
```

## Node Version

Use **Node 10**.


## Publishing

Publishing to the private NPM repository is handled by the BitBucket pipeline
when changes are merged to master.  

Prior to releasing your changes to master, you may want to update `package.json`
to indicate the next semantic version of the npm module.  Open up `package.json`
and update the `next.semver` prop (if needed) with one of the following values:
patch, minor, major.
